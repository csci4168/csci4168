﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathboxCollisions : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    void OnCollisionEnter2D(Collision2D collider)
    {
        if (collider.collider.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            //   Destroy(collider.collider.gameObject);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name); 
        }
    }
   
}
