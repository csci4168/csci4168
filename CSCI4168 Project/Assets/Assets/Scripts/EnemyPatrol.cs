﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{

    private Animator animator;
    private float moveSpeed;
    public Transform waypoint;
    public Transform player;

    //Transform nextWaypoint;
    int nextWaypoint;
    Vector2 initialPosition;
    public float currentTimeOnPath, time;
    string[] animations;

    public bool playerInRange;

    public Rigidbody2D arrow;
    public Transform arrowSpawn;
    public float arrowSpeed;
    public float timeSinceFire;

    // Use this for initialization
    void Start()
    {
        animator = GetComponent<Animator>();
        initialPosition = transform.position;
        nextWaypoint = 0;
        playerInRange = false;
        moveSpeed = 2f;
        animations = new string[] { "attack", "jump", "shoot", "dying", "run", "attack2" };
        timeSinceFire = 0;
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        timeSinceFire += Time.deltaTime;
        if (GetComponent<EnemyHealth>().health > 0)
        {
            if (!playerInRange)
            {
                patrol();
            }
            else if (timeSinceFire > 3)
            {
                attack();
            }
            else
            {
                idle();
                //if (time >= 20)
                //{/
                //  StartAnimation();
                //animator.Play(animations[Random.Range(0,animations.Length - 1)]);
                //time = 50;
                // }
            }
            isPlayerInRange();
        }
    }

    private void look(Vector2 direction)
    {
        if (direction.x > transform.position.x) // To the right
        {
            transform.localScale = new Vector3(1, transform.localScale.y, transform.localScale.z);

        }
        else if (direction.x < transform.position.x) // To the left
        {
            transform.localScale = new Vector3(-1, transform.localScale.y, transform.localScale.z);

        }
    }
    void patrol()
    {
        moveSpeed = 2f; 
        animator.Play("walk");
        Vector3 startPosition = initialPosition;
        Vector3 endPosition = waypoint.transform.GetChild(nextWaypoint).transform.position;
        // 2
        float pathLength = Vector3.Distance(startPosition, endPosition);
        float totalTimeForPath = pathLength / moveSpeed;
        //float currentTimeOnPath = Time.time - lastWaypointSwitchTime;
        currentTimeOnPath += 1 * Time.deltaTime;
        gameObject.transform.position = Vector3.Lerp(startPosition, endPosition, currentTimeOnPath / totalTimeForPath);
        // 3
        if (gameObject.transform.position.Equals(endPosition))
        {
            nextWaypoint = (nextWaypoint + 1) % waypoint.transform.childCount;
            initialPosition = transform.position;
            //waypoint = waypoint.transform.GetChild(0);
            currentTimeOnPath = 0;
            look(waypoint.transform.GetChild(nextWaypoint).position);
        }

    }
    void idle()
    {
        look(player.position);
        if (!animatorIsPlaying())
        {
            animator.Play("idle");
        }
    }
    void attack()
    {
        timeSinceFire = 0;
        look(player.position);
        animator.Play("shoot");
        Rigidbody2D projectileInstance;
        projectileInstance = Instantiate(arrow, arrowSpawn.position, arrowSpawn.rotation) as Rigidbody2D;
        projectileInstance.name = "arrow";
        projectileInstance.AddForce(transform.localScale.x * Vector3.right * arrowSpeed);
        DestroyProjectile();
    }
    void DestroyProjectile()
    {
        GameObject projectile = GameObject.Find("arrow");
        Destroy(projectile, 1.5f);
    }
    void isPlayerInRange()
    {

        if (Vector3.Distance(gameObject.transform.position, player.position) < 4f)
        {
            playerInRange = true;
        }
        else
        {
            playerInRange = false;
        }
    }
    bool animatorIsPlaying()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("shoot"))
        {
            return !(animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.75 && !animator.IsInTransition(0));
        }
        return false;
    }
}