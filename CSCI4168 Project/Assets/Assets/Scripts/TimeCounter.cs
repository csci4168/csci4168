﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeCounter : MonoBehaviour {

	public float timeRemaining = 120.0f;
	public Slider timeSlider;


	// Use this for initialization
	void Awake () {

	}

	// Update is called once per frame
	void Update () {
		timeSlider.value = timeRemaining;

        if (timeRemaining > 0)
        {
            timeRemaining -= Time.deltaTime;
        }
        else
        {
            EraseTimePickups();
        }
    }

    public void pickupTime()
    {
        if(timeRemaining + 30.0f < 120.0f)
        {
            timeRemaining += 30.0f;
        }
        else
        {
            timeRemaining = 120.0f;
        }
    }

    void EraseTimePickups()
    {
        GameObject[] timePickups = GameObject.FindGameObjectsWithTag("time_pickup");

        for (int i = 0; i < timePickups.Length; i++)
        {
            Destroy(timePickups[i]);
        }
    }
}
