﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrownSpinner : MonoBehaviour {

	public float rotateSpeed = 100.0f;
	//public GameObject solController;
	//public Transform target;

	void Start() {
		//solController = GameObject.Find("solController");
	}

	// Update is called once per frame
	void Update () {
			this.transform.Rotate(0, rotateSpeed * Time.deltaTime * -1, 0);
	}

}
