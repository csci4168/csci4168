﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartMenuButtons : MonoBehaviour {

    public Transform mainMenu, optionsMenu, instructionsMenu, musicOnImage, musicOffImage;

	// Use this for initialization
	public void SceneChanger(string newGameLevel) {

        SceneManager.LoadScene(newGameLevel);

	}
	
	public void ExitGameButton()
    {

        Application.Quit();

    }

    public void OptionsMenu(bool pressed)
    {
        if (pressed)
        {
            optionsMenu.gameObject.SetActive(pressed);
            mainMenu.gameObject.SetActive(!pressed);
        }
        else
        {
            optionsMenu.gameObject.SetActive(pressed);
            mainMenu.gameObject.SetActive(!pressed);
        }
    }

    public void InstructionsMenu(bool pressed)
    {
        if (pressed)
        {
            instructionsMenu.gameObject.SetActive(pressed);
            optionsMenu.gameObject.SetActive(!pressed);
        }
        else
        {
            instructionsMenu.gameObject.SetActive(pressed);
            optionsMenu.gameObject.SetActive(!pressed);
        }
    }

    public void MusicOnOff(bool pressed)
    {

        if (pressed)
        {
            musicOnImage.gameObject.SetActive(!pressed);
            musicOffImage.gameObject.SetActive(pressed);

        }
        else
        {
            musicOnImage.gameObject.SetActive(!pressed);
            musicOffImage.gameObject.SetActive(pressed);

        }

    }
}
