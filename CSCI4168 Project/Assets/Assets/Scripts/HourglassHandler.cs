﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HourglassHandler : MonoBehaviour {

    public GameObject pickupEffect;

	// Update is called once per frame
	private void OnCollisionEnter2D(Collision2D coll)
    {
        if(coll.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            FindObjectOfType<TimeCounter>().pickupTime();
            pickupEffect.SetActive(true);
            Instantiate(pickupEffect, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }   
}
