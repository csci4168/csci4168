﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToAttackPosition : MonoBehaviour {

 public GameObject target;
 public Vector3 currPos;
public float xScale;

	// Update is called once per frame
	void Update () {
    xScale = target.transform.localScale.x;
    currPos = target.transform.position;

    if (target.transform.localScale.x < 0) {
      transform.localScale = new Vector3(xScale,0,0);
    }
    else if (target.transform.localScale.x > 0) {
      transform.localScale = new Vector3(xScale,0,0);
    }
	}
}
