﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour {

	public float playerHealth = 100;
	public Slider healthSlider;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		healthSlider.value = playerHealth;
	}

	//method for getting hit and deducting health
}
