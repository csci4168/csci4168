﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGame : MonoBehaviour {
    public Transform pauseScreen;
    public Player player;
    public GameObject characterSwitcher;

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Pause();
        }
	}

    public void Pause()
    {
        if (!pauseScreen.gameObject.activeInHierarchy)
        {
            pauseScreen.gameObject.SetActive(true);
            Time.timeScale = 0;
            player.GetComponent<Controller2D>().enabled = false;
            player.GetComponent<PlayerInput>().enabled = false;
            player.GetComponent<Player>().enabled = false;
            characterSwitcher.SetActive(false);
        }
        else
        {
            Unpause();
        }
    }

    public void Unpause()
    {
        pauseScreen.gameObject.SetActive(false);
        Time.timeScale = 1;
        player.GetComponent<Controller2D>().enabled = true;
        player.GetComponent<PlayerInput>().enabled = true;
        player.GetComponent<Player>().enabled = true;
        characterSwitcher.SetActive(true);
    }
}
