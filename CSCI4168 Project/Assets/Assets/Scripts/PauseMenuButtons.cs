﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuButtons : MonoBehaviour {

    public Transform mainPause, volPause;
	

	public void volMenu(bool pressed)
    {
        if (pressed)
        {
            mainPause.gameObject.SetActive(!pressed);
            volPause.gameObject.SetActive(pressed);
        }
        else
        {
            mainPause.gameObject.SetActive(!pressed);
            volPause.gameObject.SetActive(pressed);
        }

    }
}
