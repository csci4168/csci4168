﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoulderSpawn : MonoBehaviour {

	public GameObject hazard;
	public float spawnWait;
	public float startWait;
	public float waveWait;

	void Start ()
	{
		StartCoroutine (SpawnWaves ());
	}

	IEnumerator SpawnWaves ()
	{
		yield return new WaitForSeconds (startWait);
		while (true)
		{
			Vector3 spawnPosition = transform.position;
			Quaternion spawnRotation = Quaternion.identity;
			GameObject boulder = Instantiate (hazard, spawnPosition, spawnRotation);
			Destroy (boulder, 20f);
			yield return new WaitForSeconds (spawnWait);
			yield return new WaitForSeconds (waveWait);
		}
	}


}
