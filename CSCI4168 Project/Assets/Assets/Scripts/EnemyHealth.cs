﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {

    public float health;
    public bool dark;
    private Animator animator;
	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
        health = 100;
	}
	
	// Update is called once per frame
	void Update () {
		if (health <= 0 && !animator.GetCurrentAnimatorStateInfo(0).IsName("dying"))
        {
            animator.Play("dying");
            //Wait until the animation finishes
        }
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("dying") && animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
        {
            Destroy(gameObject);
        }

    }

    private void OnCollisionEnter2D(Collision2D collider)
    {
      
        if (collider.collider.gameObject.layer == LayerMask.NameToLayer("SolAttack") && !dark)
            {
            health -= 35;
            Destroy(collider.collider.gameObject);
        }
        
    }
    private void OnTriggerStay2D(Collider2D collider)
    {
     if (collider.IsTouchingLayers(LayerMask.NameToLayer("enemy")) && dark)
        {
            if (PlayerInput.IsLunaAttacking())
            {
                health -= 60;
            }
        }
    }


}
