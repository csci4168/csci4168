using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Switch : MonoBehaviour {

	public static bool currChar;
	public GameObject sol;
	public GameObject luna;

	public GameObject UIsol;
	public GameObject UIluna;

	// Use this for initialization
	void Start () {
		currChar = true;
		sol = GameObject.Find("sol");
		luna = GameObject.Find("luna");
		UIsol = GameObject.Find("uisol");
		UIluna = GameObject.Find("uiluna");
	}

	// Update is called once per frame
	void Update () {
		//Vector3 currPos = sol.transform.position;
		//this.transform.position = currPos;
		if(Input.GetKeyDown(KeyCode.E))
   	{
      currChar = !currChar;
		}

		if (currChar) {
			Vector3 currPos = sol.transform.position;
			luna.transform.position = currPos;
			this.transform.position = currPos;
			sol.SetActive(true);
			luna.SetActive(false);
			UIsol.SetActive(true);
			UIluna.SetActive(false);
		}
		else {
			Vector3 currPos = luna.transform.position;
			sol.transform.position = currPos;
			this.transform.position = currPos;
			sol.SetActive(false);
			luna.SetActive(true);
			UIsol.SetActive(false);
			UIluna.SetActive(true);
		}
	}
}
