using UnityEngine;
using System.Collections;

public class SolAttack : MonoBehaviour {

    public Rigidbody2D projectile;
		public Rigidbody2D playerRB;

    public Transform Launcher;

    public float projectileSpeed = 750f;

    // Use this for initialization
    void Start () {
			playerRB = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update () {

        if (Input.GetKeyDown (KeyCode.Q))
        {
            Rigidbody2D projectileInstance;
            projectileInstance = Instantiate(projectile, Launcher.position, Launcher.rotation) as Rigidbody2D;
						projectileInstance.name = "beam attack";

						projectileInstance.AddForce(Launcher.transform.localScale.x * Vector3.right * projectileSpeed);
        }
				DestroyProjectile();
    }

		void DestroyProjectile() {
			GameObject projectile = GameObject.Find("beam attack");
			Destroy(projectile, 1.5f);
		}
}
