﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	void OnCollisionEnter2D(Collision2D collider) {
		if ( collider.collider.gameObject.tag == "Spikes" || collider.collider.gameObject.layer == LayerMask.NameToLayer("Obstacle")) {
				Destroy(this.gameObject);
			}
		}
}
