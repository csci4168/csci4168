﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakePlatform : MonoBehaviour
{

    public GameObject platform;
    public GameObject player;

    public GameObject sol;

    public Vector3 offset;

    bool platformActive = false;
    bool solActive;

    void Start()
    {
        platform.SetActive(platformActive);
    }

    // Update is called once per frame
    void Update()
    {

        solActive = CheckCharacter();

        if (Input.GetKeyDown(KeyCode.F) && solActive)
        {
            platformActive = !platformActive;
            SetPlatform();
        }

    }

    void SetPlatform()
    {
        platform.transform.position = player.transform.position + offset;
        platform.SetActive(platformActive);

    }

    bool CheckCharacter()
    {
        if (sol.activeSelf)
        {
            platform.GetComponent<Collider2D>().enabled = false;
            return true;
        }
        else
        {
            platform.GetComponent<Collider2D>().enabled = true;
            return false;
        }
    }
}
