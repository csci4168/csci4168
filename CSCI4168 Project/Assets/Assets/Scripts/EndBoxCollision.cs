﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndBoxCollision : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter2D(Collision2D collider)
    {
        if (collider.collider.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            //   Destroy(collider.collider.gameObject);
            //Load the next scene
            print("collided with " + collider.collider.gameObject.layer);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

}
