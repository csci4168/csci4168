﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Player))]
public class PlayerInput : MonoBehaviour
{
    public float health;
    public Slider healthSlider;
    private Player player;
    public Animator animationSol;
    public Animator animationLuna;
    public bool activeChar;
    private GameObject switchero;
    private bool forward;
    private bool animatingJump;
    private bool attacking;
    private float deltaTime;

    private void Start()
    {
        player = GetComponent<Player>();
        animationSol = GameObject.Find("sol").GetComponent<Animator>();
        animationLuna = GameObject.Find("luna").GetComponent<Animator>();
        healthSlider = GameObject.Find("HealthSlider").GetComponent<Slider>();

        switchero = GameObject.Find("Character Switcher");
        forward = true;
        animatingJump = false;
        attacking = false;
        health = 100;
        deltaTime = 0;
      //  healthSlider.value = 100;
    }
    private bool AnimatorIsPlaying(){
        if(activeChar)
        {
            if (animationLuna.GetCurrentAnimatorStateInfo(0).IsName("jump") || animationLuna.GetCurrentAnimatorStateInfo(0).IsName("attack"))
                return !(animationLuna.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.75 && !animationLuna.IsInTransition(0));
            else
                return !(animationLuna.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.1 && !animationLuna.IsInTransition(0));
        }
        if (animationSol.GetCurrentAnimatorStateInfo(0).IsName("jump") || animationSol.GetCurrentAnimatorStateInfo(0).IsName("casting2"))
            return !(animationSol.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.75 && !animationSol.IsInTransition(0));
        else
            return !(animationSol.GetCurrentAnimatorStateInfo(0).normalizedTime > 0.1 && !animationSol.IsInTransition(0));
     
    }

    public static bool IsLunaAttacking()
    {
        GameObject player = GameObject.Find("Player");
        PlayerInput input = player.GetComponent<PlayerInput>();
        if (input.activeChar)
        {
            return input.attacking;
        }
        return false;
    }

    private void Update()
    {
      activeChar = !Switch.currChar;
      healthSlider.value = health;

        if (activeChar)
        {
            if (health <= 0 && !animationLuna.GetCurrentAnimatorStateInfo(0).IsName("dying"))
            {
                animationLuna.Play("dying");
                //Wait until the animation finishes
            }
            if (animationLuna.GetCurrentAnimatorStateInfo(0).IsName("dying") && animationLuna.GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
        else
        {
            if (health <= 0 && !animationSol.GetCurrentAnimatorStateInfo(0).IsName("dying"))
            {
                animationSol.Play("dying");
                //Wait until the animation finishes
            }
            if (animationSol.GetCurrentAnimatorStateInfo(0).IsName("dying") && animationSol.GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
        if (health > 0)
        {
            deltaTime += Time.deltaTime;
            //If luna, heal
            if (activeChar && health < 100 && deltaTime >= 4)
                health += 10;    
            if (deltaTime > 4)
            {
                deltaTime = 0;
            }

               
            attacking = false;
            Vector2 directionalInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            player.SetDirectionalInput(directionalInput);
            if (directionalInput.x > 0)
            {
                if (!forward)
                {
                    transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
                    forward = true;
                }
                if (!AnimatorIsPlaying())
                {
                    if (activeChar)
                        animationLuna.Play("run");
                    else
                        animationSol.Play("run");
                }
            }
            else if (directionalInput.x < 0)
            {
                if (forward)
                {
                    transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
                    forward = false;
                }
                if (!AnimatorIsPlaying())
                {
                    if (activeChar)
                        animationLuna.Play("run");
                    else
                        animationSol.Play("run");
                }
            }
            else
            {
                if (!AnimatorIsPlaying() && directionalInput.y == 0)
                {
                    if (activeChar)
                        animationLuna.Play("idle");
                    else
                        animationSol.Play("idle");
                }
            }

            if (Input.GetButtonDown("Jump"))
            {
                animatingJump = true;
                if (activeChar)
                    animationLuna.Play("jump");
                else
                    animationSol.Play("jump");
                player.OnJumpInputDown();
            }

            if (Input.GetButtonUp("Jump"))
            {
                animatingJump = false;
                player.OnJumpInputUp();
                //animationLuna.Play("jump");
                //animationSol.Play("jump");
            }

            if (Input.GetKeyDown(KeyCode.Q))
            {
                if (activeChar)
                {
                    animationLuna.Play("attack");
                    attacking = true;
                }
                else
                    animationSol.Play("casting2");
            }
        }

        

    }

    void OnCollisionEnter2D(Collision2D collider)
    {
        print("Hit by arrow");
        if (collider.collider.gameObject.layer == LayerMask.NameToLayer("EnemyAttack"))
        {
            health -= 10;
        }
		if (collider.collider.gameObject.layer == LayerMask.NameToLayer("Boulder"))
		{
			health = 0;
		}
    }
}
